import { Component, OnInit } from '@angular/core';

import { Leader } from '../share/leader';
import { LeaderService } from '../services/leader.service';

import { Params, ActivatedRoute } from '@angular/router';
import {Dish} from '../share/dish';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent implements OnInit {
  selectedLeader: Leader;
  leaders: Leader[];

  constructor(private leaderService: LeaderService) { }

  ngOnInit(): void {
    this.leaders = this.leaderService.getLeaders();
  }
  onSelect(leader: Leader){
    this.selectedLeader = leader;
  }

}
