import { Promotion } from './promotion';

export const PROMOTIONS: Promotion[] = [
  {
    id: '0',
    name: 'Lomo Saltado',
    image: '/assets/images/lomo.jpg',
    label: 'New',
    price: '19.99',
    featured: true,
    // tslint:disable-next-line:max-line-length
    description: 'Lomo Saltado solo del 10-14 de agosto, llevalo en combo con una crema volteada a solo S/.18'
  }
];
