import { Injectable } from '@angular/core';
import {Dish} from '../share/dish';
import  {PROMOTIONS} from '../share/dishes';

@Injectable({
  providedIn: 'root'
})
export class DishService {

  constructor() { }

  getDishes(): Dish[] {
    return PROMOTIONS;
  }
  getDish(id: string): Dish {
    return PROMOTIONS.filter((dish) => (dish.id === id))[0];
  }
  getFeaturedDish(): Dish{
    return PROMOTIONS.filter((dish) => dish.featured)[0];
  }
}
